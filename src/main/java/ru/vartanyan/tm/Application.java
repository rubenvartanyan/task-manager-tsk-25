package ru.vartanyan.tm;

import org.jetbrains.annotations.NotNull;
import ru.vartanyan.tm.bootstrap.Bootstrap;

public class Application {

    public static void main(String[] args) throws Throwable {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}
