package ru.vartanyan.tm.exception.empty;

public class EmptyRoleException extends Exception{

    public EmptyRoleException() throws Exception {
        super("Error! Role cannot be null or empty...");
    }

}
