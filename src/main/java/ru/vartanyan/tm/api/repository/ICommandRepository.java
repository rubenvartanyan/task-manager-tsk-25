package ru.vartanyan.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.command.AbstractCommand;
import java.util.Collection;
import java.util.List;

public interface ICommandRepository {

    @NotNull
    Collection<AbstractCommand> getCommand();

    @NotNull
    Collection<AbstractCommand> getArguments();

    @NotNull
    Collection<String> getCommandNames();

    @NotNull
    Collection<String> getCommandsArgs();

    @Nullable
    AbstractCommand getCommandByName(@NotNull final String name);

    @Nullable
    AbstractCommand getCommandByArg(@NotNull final String arg);

    void add(@NotNull final AbstractCommand command);

    @Nullable
    List<AbstractCommand> getCommandList();

}
