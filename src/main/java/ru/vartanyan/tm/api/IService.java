package ru.vartanyan.tm.api;

import ru.vartanyan.tm.model.AbstractEntity;

public interface IService<E extends AbstractEntity> extends IRepository<E> {
}
