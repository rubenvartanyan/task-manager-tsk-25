package ru.vartanyan.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.InputStream;
import java.util.Properties;

public class PropertyService implements ru.vartanyan.tm.api.IPropertyService {

    @NotNull
    public static final String FILE_NAME = "application.properties";

    @NotNull
    private static final String PASSWORD_SECRET_KEY = "secret";

    @NotNull
    private static final String PASSWORD_SECRET_DEFAULT = "";

    @NotNull
    private static final String DEVELOPER_NAME_KEY = "name";

    @NotNull
    private static final String DEVELOPER_NAME_DEFAULT = "";

    @NotNull
    private static final String DEVELOPER_EMAIL_KEY = "email";

    @NotNull
    private static final String DEVELOPER_EMAIL_DEFAULT = "";

    @NotNull
    private static final String PASSWORD_ITERATION_KEY = "iteration";

    @NotNull
    private static final String PASSWORD_ITERATION_DEFAULT = "1";

    @NotNull
    private static final String APPLICATION_VERSION_KEY = "version";

    @NotNull
    private static final String APPLICATION_VERSION_DEFAULT = "";

    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        @Nullable final InputStream inputStream = ClassLoader.getSystemResourceAsStream(FILE_NAME);
        if (inputStream == null) return;
        properties.load(inputStream);
        inputStream.close();
    }

    @Override
    public @NotNull String getPasswordSecret() {
        if (System.getenv().containsKey(PASSWORD_SECRET_KEY)) {
            return System.getProperty(PASSWORD_SECRET_KEY);
        }
        if (System.getProperties().containsKey(PASSWORD_SECRET_KEY)) {
            return System.getProperty(PASSWORD_SECRET_KEY);
        }
        return properties.getProperty(PASSWORD_SECRET_KEY, PASSWORD_SECRET_DEFAULT);
    }

    @NotNull
    @Override
    public Integer getPasswordIteration() {
        if (System.getenv().containsKey(PASSWORD_ITERATION_KEY)) {
            final String value = System.getProperty(PASSWORD_ITERATION_KEY);
            return Integer.parseInt(value);
        }
        if (System.getProperties().containsKey(PASSWORD_ITERATION_KEY)) {
            final String value = System.getProperty(PASSWORD_ITERATION_KEY);
            return Integer.parseInt(value);
        }
        final String value = properties.getProperty(PASSWORD_ITERATION_KEY, PASSWORD_ITERATION_DEFAULT);
        return Integer.parseInt(value);
    }

    public static void main(String[] args) {
        final PropertyService service = new PropertyService();
        System.out.println(service.getPasswordSecret());
    }

    @NotNull
    @Override
    public String getApplicationVersion() {
        if (System.getenv().containsKey(APPLICATION_VERSION_KEY)) {
            return System.getProperty(APPLICATION_VERSION_KEY);
        }
        if (System.getProperties().containsKey(APPLICATION_VERSION_KEY)) {
            return System.getProperty(APPLICATION_VERSION_KEY);
        }
        return properties.getProperty(APPLICATION_VERSION_KEY, APPLICATION_VERSION_DEFAULT);
    }

    @NotNull
    @Override
    public String getDeveloperName() {
        if (System.getenv().containsKey(DEVELOPER_NAME_KEY)) {
            return System.getProperty(DEVELOPER_NAME_KEY);
        }
        if (System.getProperties().containsKey(DEVELOPER_NAME_KEY)) {
            return System.getProperty(DEVELOPER_NAME_KEY);
        }
        return properties.getProperty(DEVELOPER_NAME_KEY, DEVELOPER_NAME_DEFAULT);
    }

    @NotNull
    @Override
    public String getDeveloperEmail() {
        if (System.getenv().containsKey(DEVELOPER_EMAIL_KEY)) {
            return System.getProperty(DEVELOPER_EMAIL_KEY);
        }
        if (System.getProperties().containsKey(DEVELOPER_EMAIL_KEY)) {
            return System.getProperty(DEVELOPER_EMAIL_KEY);
        }
        return properties.getProperty(DEVELOPER_EMAIL_KEY, DEVELOPER_EMAIL_DEFAULT);
    }

}
