package ru.vartanyan.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.api.repository.ICommandRepository;
import ru.vartanyan.tm.api.service.ICommandService;
import ru.vartanyan.tm.command.AbstractCommand;

import java.util.Collection;
import java.util.List;

public class CommandService implements ICommandService {

    @NotNull private final ICommandRepository commandRepository;

    public CommandService(@NotNull final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Nullable
    @Override
    public AbstractCommand getCommandByName(@NotNull String name) {
        return commandRepository.getCommandByName(name);
    }

    @Nullable
    @Override
    public AbstractCommand getCommandByArg(@NotNull String arg) {
        return commandRepository.getCommandByArg(arg);
    }

    @Nullable
    @Override
    public Collection<AbstractCommand> getArguments() {
        return commandRepository.getArguments();
    }

    @Nullable
    @Override
    public Collection<AbstractCommand> getCommands() {
        return commandRepository.getCommand();
    }

    @Nullable
    @Override
    public Collection<String> getListArgumentName() {
        return commandRepository.getCommandsArgs();
    }

    @Nullable
    @Override
    public Collection<String> getListCommandName() {
        return commandRepository.getCommandNames();
    }

    @Override
    public void add(@Nullable final AbstractCommand command) {
        if (command == null) return;
        commandRepository.add(command);
    }

    @Nullable
    @Override
    public List<AbstractCommand> getCommandList() {
        return commandRepository.getCommandList();
    }

}
