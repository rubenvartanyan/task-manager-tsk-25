package ru.vartanyan.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.vartanyan.tm.command.AbstractCommand;

public class AboutCommand extends AbstractCommand {

    @Override
    public String arg() {
        return "-a";
    }

    @Override
    public @NotNull String name() {
        return "show-about";
    }

    @Override
    public String description() {
        return "Show about";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[ABOUT]");
        System.out.println("NAME: " + serviceLocator.getPropertyService().getDeveloperName());
        System.out.println("E-MAIL: " + serviceLocator.getPropertyService().getDeveloperEmail());
    }

}
