package ru.vartanyan.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.vartanyan.tm.command.AbstractProjectCommand;
import ru.vartanyan.tm.exception.system.NotLoggedInException;
import ru.vartanyan.tm.model.Project;
import ru.vartanyan.tm.util.TerminalUtil;

public class ProjectFinishByIdCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "project-finish-by-id";
    }

    @Override
    public String description() {
        return "Finish project by Id";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[FINISH PROJECT]");
        if (serviceLocator.getAuthService().isNotAuth()) throw new NotLoggedInException();
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[ENTER ID]");
        @NotNull final String id = TerminalUtil.nextLine();
        serviceLocator.getProjectService().finishEntityById(id, userId);
        System.out.println("[PROJECT FINISHED]");
    }

}
